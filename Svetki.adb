with Text_IO;
use Text_IO;


-- Autors: Matiss Zervens
-- Izveidosanas datums: 12.12.2015
-- Labojuma datums: 03.01.2016

-- Veiktie labojumi:
-- Nebiju sapratis lidz galam tasku paralelo izpildi, parnesu start entry logiku uz FatherChristmas taska kermeni, tadejadi notiek paralela izpilde.
-- Papildus tika izlabota maza kluda, kura taski ieciklojas, ja ir savakti visi viena veida resursi, bet otri ir beigusies


procedure Svetki is

   type IntBool is
      record
         val: Integer;
         b: Boolean;
      end record;

   type FindPriority is (COOKIES, SWEETS);

   type RequestResult is
      record
         val: Integer;
         b: Boolean;
      end record;


   task type Box is
      entry finish;
      entry getSweets(item: out RequestResult);
      entry getCookies(item: out RequestResult);
   end Box;

   task body Box is
      sweets: array (1 .. 7) of IntBool;
      sweetsTaken: Boolean := False;
      cookies: array (1 .. 11) of IntBool;
      cookiesTaken: Boolean := False;

      endTask: Boolean := False;
   begin

      -- Initializes the contents of the box
      for i in sweets'Range loop
         sweets(i) := (val => i * 100,
                       b => True);
      end loop;

      for i in cookies'Range loop
         cookies(i) :=  (val => i * 100 + 200,
                         b => True);
      end loop;

      while (endTask /= True) loop
         select
            -- Returns sweets if there are any left
            accept getSweets(item: out RequestResult) do
               if(sweetsTaken) then
                  item := (0, False);
               else
                  for i in reverse sweets'Range loop
                     if(sweets(i).b) then
                        sweets(i).b := False;
                        item := (sweets(i).val, True);

                        if(i = sweets'First) then
                           sweetsTaken := True;
                        end if;
                        exit;
                     end if;
                  end loop;
               end if;
            end getSweets;

         or
            -- Returns cookies if there are any left
            accept getCookies(item : out RequestResult) do
               if(cookiesTaken) then
                  item := (0, False);
               else
                  for i in reverse cookies'Range loop
                     if(cookies(i).b) then
                        item := (cookies(i).val, True);
                        cookies(i).b := False;

                        if(i = cookies'First) then
                           cookiesTaken := True;
                        end if;
                        exit;
                     end if;
                  end loop;
               end if;
            end getCookies;

         or
            -- Terminates the task
            accept finish  do
               --Put_Line("a box terminating");
               endTask := True;
            end finish;

         end select;
      end loop;
   end Box;
   boxes: array(1 .. 10) of Box;



   task type FatherChristmas is
      entry Init(p: FindPriority; findCookies, findSweets: Integer; myId: String);
      entry HasFinished(status: out Boolean);
      entry finish;
   end FatherChristmas;

   task body FatherChristmas is
      priority: FindPriority;
      wantsCookies: Integer;
      wantsSweets: Integer;

      hasCookies: array (1 .. 14) of Integer := (others => 0);
      hasSweets: array (1 .. 9) of Integer := (others => 0);
      cookiesTaken: Integer := 0;
      sweetsTaken: Integer := 0;

      finishedStatus: Boolean := False;

      endTask: Boolean := False;

      cookiesEnded: Boolean := False;
      sweetsEnded: Boolean := False;

      totalCookies: Integer := 0;
      totalSweets: Integer := 0;

      currentBox: Integer := boxes'First;
      request: RequestResult;

      changeOccured: Boolean := False;

      iterationsComplete: Integer := 0;

      id: String := "00";
   begin

      accept Init (p: in FindPriority; findCookies, findSweets: in Integer; myId: in String) do
         priority := p;
         wantsCookies := findCookies;
         wantsSweets := findSweets;
         id := myId;
      --   Put_Line("santa init " & id);
      end Init;

      loop
       --  Put_Line("santa looping " & id);
         if((priority = SWEETS) and (sweetsTaken /= wantsSweets) and (sweetsEnded = False)) then
            boxes(currentBox).getSweets(request);
            if(request.b) then
               hasSweets(sweetsTaken + 1) := request.val;
               sweetsTaken := sweetsTaken + 1;
               changeOccured := True;
            end if;
         elsif((priority = COOKIES) and (cookiesTaken /= wantsCookies) and (cookiesEnded = False)) then
            boxes(currentBox).getCookies(request);
            if(request.b) then
               hasCookies(cookiesTaken + 1) := request.val;
               cookiesTaken := cookiesTaken + 1;
               changeOccured := True;
            end if;
         end if;

         iterationsComplete := iterationsComplete + 1;
         currentBox := currentBox + 1;

         if((priority = SWEETS) and (sweetsTaken = wantsSweets)) then
            priority := COOKIES;
            iterationsComplete := 0;
            if(cookiesEnded) then
               sweetsEnded := True;
            end if;

         elsif((priority = COOKIES) and (cookiesTaken = wantsCookies)) then
            priority := SWEETS;
            iterationsComplete := 0;

            if(sweetsEnded) then
               cookiesEnded := True;
            end if;
         end if;


         if(currentBox > boxes'Last) then
            currentBox := boxes'First;
         end if;

         if(iterationsComplete = boxes'Last) then
            if(changeOccured = False) then
               if((priority = SWEETS)) then
                  sweetsEnded := True;
                  priority := COOKIES;
               elsif((priority = COOKIES)) then
                  cookiesEnded := True;
                  priority := SWEETS;
               end if;
            else
               changeOccured := False;
            end if;
            iterationsComplete := 0;
         end if;


         if((cookiesTaken = wantsCookies) and (sweetsTaken = wantsSweets)) then

            for i in hasCookies'Range loop
               totalCookies := totalCookies + hasCookies(i);
            end loop;

            for i in hasSweets'Range loop
               totalSweets := totalSweets + hasSweets(i);
            end loop;

            Put_Line("Sweets in grams            : " & Integer'Image(totalSweets) &
                       " Cookies in grams        : " & Integer'Image(totalCookies) &
             " (My id is " & id & " )");

            finishedStatus := True;
            exit;
         elsif(cookiesEnded and sweetsEnded) then

            for i in hasCookies'Range loop
               totalCookies := totalCookies + hasCookies(i);
            end loop;

            for i in hasSweets'Range loop
               totalSweets := totalSweets + hasSweets(i);
            end loop;

            Put_Line("Sweet packages missing     : " & Integer'Image(wantsSweets - sweetsTaken) &
                       " Cookie packages missing : " & Integer'Image(wantsCookies - cookiesTaken) &
              " (My id is " & id & " )" &
              " (Sweets                        : " & Integer'Image(totalSweets) &
              " Cookies                        : " & Integer'Image(totalCookies) & ")");

            finishedStatus := True;
            exit;

         end if;
      end loop;


      while (endTask /= True) loop
         select

            -- Returns tasks status
            accept HasFinished (status: out Boolean) do
               status := finishedStatus;
            end HasFinished;

         or

            -- Terminates tasks execution
            accept finish  do
               --Put_Line("hohoho terminating");
               endTask := True;
            end finish;


         end select;
      end loop;
   end FatherChristmas;


   hohohos: array(1 .. 8) of FatherChristmas;
   hasFinished: Boolean;
begin

   -- Initializing Santas and start gathering stuff
   hohohos(1).Init(SWEETS, 14, 9, Integer'Image(1));
   hohohos(5).Init(SWEETS, 14, 9, Integer'Image(5));
   hohohos(8).Init(SWEETS, 14, 9, Integer'Image(8));

   for i in 2 .. 4 loop
      hohohos(i).Init(COOKIES, 14, 9, Integer'Image(i));
   end loop;

   for i in 6 .. 7 loop
      hohohos(i).Init(COOKIES, 14, 9, Integer'Image(i));
   end loop;




   -- Once all Santas have finished we terminate all tasks and exit the main task.
   loop

      for i in hohohos'Range loop
         hohohos(i).HasFinished(hasFinished);
         if(hasFinished = False) then
            exit;
         end if;

      end loop;

      if(hasFinished) then
         for i in hohohos'Range loop
            hohohos(i).finish;
         end loop;

         for i in boxes'Range loop
            boxes(i).finish;
         end loop;

         exit;
      end if;

   end loop;

end Svetki;
